## instalacion de python
    python3 --version
## instalacion de node
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
    source ~/.bashrc
    nvm list-remote
    nvm install v20.0.0
    nvm list

    node --version
## creo un entorno para el proyecto con el nombre venv
    python3 -m venv venv
## activo el entorno creado
    source venv/bin/activate
## instal django
    pip install django
## inicio el proyecto a la altura de la carpeta de el entorno
    django-admin startproject djago_crud_api .
## creo una nueva app para mi proyecto llamado tasks
    python manage.py startapp tasks
    add into 'INSTALLED_APPS' : 'tasks',
## creo las tablas en base de datos
    python manage.py migrate
## instalo rest-framework
    pip install djangorestframework
    add into 'INSTALLED_APPS' : 'rest_framework',
## instalo para cors
    pip install django-cors-headers
    add into 'INSTALLED_APPS' : 'corsheaders',
    add into 'MIDDLEWARE' before: 'django.middleware.common.CommonMiddleware' : 'corsheaders.middleware.CorsMiddleware',
    add finish settings.py: CORS_ALLOWED_ORIGINS = []

## creo la estructura de la tabla para los datos de las tasks
    En el archivo tasks/models.py:
    '''
        from django.db import models

        # Create your models here.
        class Task(models.Model):
        title = models.CharField(max_length=200)
        description = models.TextField(blank=True)
        done = models.BooleanField(default=False)
    '''
## corro las migraciones
    python manage.py makemigrations
## creo las tablas en bbdd de la app tasks
    python manage.py migrate tasks
## creo un usuario admin
    python manage.py createsuperuser // admin, liker251281@gmail.com , 20ri8125
## inicio el servidor
    python manage.py runserver
## para poder visualizar la app tasks en el admin entro en tasks/admin y agrego:
    '''
    from django.contrib import admin
    from .models import Task

    # Register your models here.
    admin.site.register(Task)

    def __str__(self) -> str:
        return self.title
    '''
## Para poder serializar lo que devuelve la bbdd con rest framework y que pueda ser enviado a un frontend, cre el archivo ./tasks/resializer.py
    '''
    from rest_framework import serializers
    from .models import Task

    class TaskSerializer(serializers.ModelSerializer):
        class Meta:
            model = Task
            # fields = ('id', 'title', 'description', 'done')
            fields = '__all__'
    '''
## Para las rutas de la api, creo el archivo ./tasks/urls.py
    '''
    from django import path, include
    from rest_framework import routers
    from tasks import views

    # api versioning
    router = routers.DefaultRouter()
    router.register(r'tasks', views.TaskView, 'tasks')

    urlpatterns = [
        path("api/v1/", include(router.urls))
    ]
    '''
## Luego tengo que publicar esas rutas creadas en al app, edito el archivo ./django_crud_api/urls.py
    '''
    from django.contrib import admin
    from django.urls import path, include

    urlpatterns = [
        path('admin/', admin.site.urls),
        path('tasks/', include('tasks.urls')),
    ]
    '''
    Con esto puedo desde el navegador visitar: http://127.0.0.1:8000/tasks/api/v1/tasks/ para ver el listado de tasks
## agregamos la documentacion de la api
    pip install coreapi
    add into 'INSTALLED_APPS' : 'coreapi',
    agrego una ruta nueva en ./tasks/urls.py
        path("docs/", include_docs_urls(title="Task API")),
    agrego un array en ./django_crud_api/settings.py
        REST_FRAMEWORK = {
            'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema',
        }
    Con esto puedo ir a la documentacion: http://127.0.0.1:8000/tasks/docs/
## para ver la parte del admin
    http://127.0.0.1:8000/admin/


