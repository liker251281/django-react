import { useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { getTask, createTask, updateTask, deleteTask } from '../api/tasks.api'
import { useNavigate, useParams } from 'react-router-dom'

export const TaskFormPage = () => {
    const {
        register,
        handleSubmit,
        formState: { errors },
        setValue,
    } = useForm()

    const navigate = useNavigate()
    const params = useParams()

    const onSubmit = handleSubmit(async (data) => {
        if (params.id) {
            await updateTask(params.id, data)
        } else {
            await createTask(data)
        }
        navigate('/tasks')
    })

    useEffect(() => {
        async function loadTask () {
            if (params.id) {
                const { data } = await getTask(params.id)
                setValue('title', data.title)
                setValue('description', data.description)
            }
        }
        loadTask()
    }, [])

    return (
        <div>
            <form onSubmit={ onSubmit }>
                <input
                    type="text"
                    placeholder="Title"
                    {...register('title', { required: true })}
                />
                {errors.title && <span>title is required</span>}

                <textarea
                    rows="3"
                    placeholder="Description"
                    {...register('description', { required: true })}
                />
                {errors.description && <span>description is required</span>}

                <button>Save</button>
            </form>

            {
                params.id &&
                    <button
                        onClick={async () => {
                            const accepted = window.confirm('Are you sure?')
                            if(accepted) {
                                await deleteTask(params.id)
                                navigate('/tasks')
                            }
                        }}
                    >
                        Delete
                    </button>
            }
        </div>
    )
}